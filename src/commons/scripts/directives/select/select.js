


(function () {
    'use strict';



    function selectedDrop ( $compile, ModalSrv, AppConsSrv, $location ) {
        return {
            restrict : 'A',
            //replace : true,
            scope : {
                ngModel : '=',
                selectionate : "=",
                afterSelect : "=",
                placeholder : "@",
                hasError : "=",
                ngDisabled : '=',
                firstOption: '@',
                ngClick : '=',
                id : '@'
            },
            link : function ( $scope, $element, $attrs ) {

                var element;


                function setup(){

                    setupVars();
                    if( $scope.cons.devices && window.device === $scope.cons.devices.MOBILE ){
                        setupElementMobile();
                    }
                    else{
                        setupElementDesktop();
                    }
                }



                $scope.click = function(){
                    $scope.filtro = "";
                    if( angular.isFunction( $scope.ngClick ) ){
                        $scope.ngClick();
                    }
                };



                $scope.updateMobile =  function(){
                    $scope.itemSelect = angular.fromJson($scope.itemSelect);
                    if( $scope.itemSelect && $scope.itemSelect.url ){
                        sendModal(  $scope.itemSelect );
                    }
                    else if( $scope.itemSelect ) {
                        $scope.selectionate.selected = $scope.itemSelect;
                        if( angular.isFunction($scope.afterSelect) ){
                            $scope.afterSelect(  angular.fromJson($scope.selectionate.selected) );
                        }
                    }
                };



                $scope.update = function( _item ){
                    if( _item.url ){
                        sendModal( _item );
                    }
                    else{
                        $scope.hideOptions = true;
                        $scope.selectionate.selected = _item;

                        if( angular.isFunction($scope.afterSelect) ){
                            $scope.afterSelect( _item );
                        }
                    }
                };



                function sendModal( _item ) {
                    var stepModal = $location.path();
                    stepModal = stepModal.replace("/","").split("-")[0];
                    stepModal = stepModal.search("paso") !== -1 ? stepModal : "";

                    // ModalSrv.openChangeProduct($scope)
                    // .then(function (){
                    //     ldOmnitureSrv.trackingEvent('changeQuotation', {
                    //         path: $location.path(),
                    //         srcPageTracking: true,
                    //         enlace: stepModal + ':cotizar otro producto',
                    //         productSelect: _item.descripcionInterna
                    //     });
                    //     window.location.href = _item.url;
                    // });
                }



                function setupElementMobile(){
                    var itemSelect = "descripcionInterna";

                    var elementsRemove = $($element).removeAttr("selected-drop");
                    //elementsRemove.attr("ng-options","y['"+itemSelect+"'] for y in ngModel");
                    elementsRemove.attr("ng-change","updateMobile()");
                    elementsRemove.attr("ng-model","itemSelect");
                    elementsRemove.removeAttr("ng-click");
                    var txt= $element[0].outerHTML;

                    txt = txt.replace(/<option .*><\/option>/gi, '<option value="" ng-show="!placeholder"> {{placeholder}}</option>');
                    txt = txt.replace('><','><option value="" ng-show="firstOption"> {{firstOption}}</option><option ng-repeat="y in ngModel" value="{{y}}" id="{{ id }}-{{ y.descripcionInterna | sanatizeFtr }} "> {{y.descripcionInterna}} </option><');


                    var template = '<div class="btn-group" uib-dropdown is-open="status.isopen" ng-class="{\'open\' : status.isopen, \'error\' : hasError }" ng-click="ngClick()">'+
                      '<label id="single-button" type="button" class="btn btn-primary" uib-dropdown-toggle ng-disabled="ngDisabled">'+
                        '<span>{{ selectionate.selected.descripcionInterna ? selectionate.selected.descripcionInterna : placeholder }}</span>'+
                        txt +
                        '<span ng-show="hasError" class="wibe-icon icon-error"></span>'+
                        '<span class="wibe-icon icon-arrow"></span>'+
                      '</label>'+
                    '</div>';

                    $( $element ).wrap( '<div class="selected-drop"></div>' );
                    $( $element ).parent( '.selected-drop' ).prepend( template );


                    element = $( $element ).parent( '.selected-drop' );
                    $($element ).remove();
                    $compile( element.contents() )( $scope );
                }



                function setupElementDesktop () {
                    var itemSelect = "descripcionInterna";

                    var repeat = 'ng-repeat="item in ngModel |  filter:{'+ itemSelect +':filtro }:false"';

                    var option = '<span class="container-option" ng-class="{\'no-img\': !item.img }">'+
                        '<span ng-if="item.img" class="img {{item.img}}"></span>'+
                        '<span>{{ item["'+itemSelect+'"]}}</span>'+
                    '</span>';
                    var opctionContainer = '<li '+ repeat +' ng-click="update( item )" role="menuitem">'+ option +'</li>';


                    $( $element ).wrap( '<div class="selected-drop"></div>' );
                    $( $element ).parent( '.selected-drop' )
                    .prepend(
                        '<div class="btn-group" uib-dropdown is-open="status.isopen" ng-class="{\'open\' : status.isopen , \'error\' : hasError }" ng-click="click()">'+
                          '<label id="single-button" type="button" class="btn btn-primary" uib-dropdown-toggle ng-disabled="ngDisabled">'+
                            '<span> {{ selectionate.selected.descripcionInterna ? selectionate.selected.descripcionInterna : placeholder }}</span>'+
                            '<input id="search" ng-show="status.isopen" placeholder="{{ placeholder }}" ng-model="filtro"/>'+
                            '<span ng-show="hasError" class="wibe-icon icon-error"></span>'+
                            '<span class="wibe-icon icon-arrow"></span>'+
                          '</label>'+
                          '<ul class="uib-dropdown-menu" role="menu" aria-labelledby="single-button" scrollable>'+
                                opctionContainer+
                          '</ul>'+
                        '</div>'
                    );
                    element = $( $element ).parent( '.selected-drop' );

                    $($element ).remove();

                    $compile( element.contents() )( $scope );

                    element.find("label").click(function(){
                        var rsssss= $(this);
                        setTimeout(function(){
                            rsssss.find("#search").focus();
                        },400);
                    });
                }



                function setupVars(){
                    if( $scope.selectionate && $scope.selectionate.selected ){
                        $scope.selectionate = $scope.selectionate;
                    }
                    else{
                        $scope.selectionate = {
                            selected: false
                        };
                    }
                    $scope.status = {
                        isopen: false
                    };
                    $scope.cons = new AppConsSrv();
                }



                setup();
            }
        };
    }



    angular.module( 'commons.directives' )
        .directive( 'selectedDrop', selectedDrop );
}());
