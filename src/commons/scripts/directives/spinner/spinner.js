


(function () {
    'use strict';



    function spinner () {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'directives/spinner/spinner.tpl.html',
            scope: {
                show: '='
            }
        };
    }



    angular.module( 'commons.directives' )
        .directive( 'spinner', spinner );
}());
