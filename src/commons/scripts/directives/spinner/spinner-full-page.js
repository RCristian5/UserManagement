


(function () {
    'use strict';



    function spinnerFullPage () {
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'directives/spinner/spinner-full-page.tpl.html',
            scope: {
                show: '='
            },
            link: LinkSpinnerFullPage
        };
    }



    function LinkSpinnerFullPage ($scope) {



        function setup () {
            setupWatchShowSpinner();
        }



        function setupWatchShowSpinner () {
            $scope.$watch( 'show', function () {

                if ( $scope.show ) {
                    lockBodyScroll( true );
                } else {
                    lockBodyScroll( false );
                }
            } );
        }



        function lockBodyScroll ( _lockBodyScroll ) {
            $( 'body' ).css( {
                'overflow': ( _lockBodyScroll ? 'hidden' : 'auto' )
            });
        }



        setup();
    }



    angular.module( 'commons.directives' )
        .directive( 'spinnerFullPage', spinnerFullPage );
}());
