# Uso de la directiva de spinner


## spinnner-full-page

```html
<div spinner-full-page="" show="flag"></div>
```

**en donde**

**spinner-full-page:** es la instancia de la directiva

**show:** es la bandera que dirá si se muestra o no el spinner

## spinnner
```html
<div spinner"" show="flag"></div>
```

**en donde**

**spinner:** es la instancia de la directiva

**show:** es la bandera que dirá si se muestra o no el spinner