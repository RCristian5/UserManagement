


(function () {
    'use strict';



    function errorArea ( $compile, $timeout ) {



        return {
            replace: true,
            restrict: 'EA',
            template: '<div ng-transclude=""></div>',
            transclude: true,
            scope: {
                errorArea: "="
            },
            link: function ( $scope, $element ) {
                var promiseErrorArea = null;
                var showBlink = false;



                function setup(){
                    setupVars();
                    buildErrorArea();
                }



                function timeHideErrorArea () {
                    if( promiseErrorArea ){
                        $timeout.cancel( promiseErrorArea );
                    }

                    if( $scope.messages.length > 0 ){
                        showBlink = true;
                    }

                    promiseErrorArea = $timeout(function () {
                        $scope.messages = [];
                        promiseErrorArea = null;
                        showBlink = false;
                    }, 5000);
                }



                function setBlinking( ) {
                    if( showBlink ){
                        $scope.blink = true;
                        $timeout(function () {
                            $scope.blink = false;
                        }, 1000);
                    }
                }



                function setupVars () {
                    $scope.messages = [];
                    $scope.blink = false;
                    $scope.errorArea = {
                        pop : pop,
                        push: push,
                        clear: clear
                    };
                }



                function push( _message ){
                    var pushToMessage = existsMessage( _message );
                    if( !pushToMessage){
                        $scope.messages.push( _message );
                    }

                    showErrorAreaWithScroll();
                    setBlinking( );
                    timeHideErrorArea();
                }



                function pop( _message ){
                    if( _message ){
                        var messagePop = existsMessage( _message );
                        if( messagePop ){

                            var idInArray = 0;
                            _.find( $scope.messages ,function( _msg, _index ){
                                if( _message === _msg){
                                    idInArray = _index;
                                    return _msg;
                                }
                            });
                            $scope.messages.splice( idInArray, 1 );
                        }
                    }
                }



                function clear(){
                    $scope.messages = [];
                }



                function existsMessage ( _messageToVerify ) {
                    var repeatedMessage = _.find( $scope.messages, function ( _message ) {
                        if ( _messageToVerify === _message ) {
                            return _message;
                        }
                    } );

                    return repeatedMessage;
                }



                function buildErrorArea (  ) {
                    $element.append( '<div class="block-errors-managed" ng-class="{ \'hide-error\': ( messages.length <= 0 ), \'blink-me\': ( blink ) }"><ul><li ng-repeat="message in messages"><span class="message-dot">&#183;</span><span class="message-content">{{ message }}</span></li></ul></div>' );
                    $compile( $element.contents() )( $scope );
                }



                function showErrorAreaWithScroll ( ) {
                    var positionToScroll = 0;
                    positionToScroll = $element.offset().top - 60;
                    if( !$('body').hasClass("modal-open") ){
                        $('body').animate({
                            scrollTop: positionToScroll
                        }, 500 );
                    }
                }


                setup();
            }
        };
    }



    angular.module( 'commons.directives' )
        .directive( 'errorArea', errorArea );
}());
