# Radio Buttons

### Para hacer uno de la directiva "radio-button"
Insertar

```html
<input type="radio" radio-button="" is-checked="" on-click="" ng-disabled="" >
```

donde

  - radio-button="" - Es el atributo para insetar la directiva y dentro de las comillan ira el texto a mostrar como etiqueta.
  - on-click="" - Aca se le pasa el nombre del la funcion definida en el scope del controlador podiendo pasar parametros y sera la que se ejecutara al darle click al radio-button.
  - is-checked="" - Condicion que se debe de cumplir para que que el radio button sea seleccionado.
  - ng-disabled="" - Se le puede mandar el valor "true" para desabilitarlo o "false" para caso contrario, si se evita este atributo se tomara con un valor "false".

Se insertar dentro de una <input type="radio"> para hacer referencia que es un radio-buttons que se insertara con la directiva.


