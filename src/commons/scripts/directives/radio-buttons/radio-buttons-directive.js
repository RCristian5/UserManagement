(function(){
    'use strict';


    function radioButtons() {



        return {
            restrict: 'A',
            replace: true,
            templateUrl:  'directives/radio-buttons/radio-buttons.tpl.html',
            scope:{
                radioButtons:  "@",
                onClick:       "&",
                ngDisabled:    "=",
                isChecked:     "=",
            }
        };



    }



    angular.module( 'commons.directives' )
    .directive( 'radioButtons', radioButtons );



}());
