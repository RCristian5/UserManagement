


(function () {
    'use strict';



    function wbInputState ( $compile ) {
        return {
            restrict: 'A',
            transclude: true,
            require: [ 'ngModel' ],
            scope: {
                ngModel: '=',
                wbInputState: '@',
                wbiShowSuccessIcon: '=',
                wbiTooltip: '@',
                wbiTooltipPlacement: '@'
            },
            link: function ( $scope, $element, $attrs ) {



                function setup () {
                    setupVars();
                    setupElementClass();
                    setupWatchModel();
                }



                function setupVars () {

                    if ( ! $scope.wbInputState ) {
                        $scope.wbInputState = 'NONE';
                    }

                    if ( $scope.wbiShowSuccessIcon === undefined || $scope.wbiShowSuccessIcon === null ) {
                        $scope.wbiShowSuccessIcon = false;
                    }
                }



                function setupElementClass () {

                    $( $element ).wrap( '<div class="wb-input-state"></div>' );

                    $( $element ).parent( '.wb-input-state' ).prepend( '<span class="status-container">' +
                        '<span class="input-state-tooltip" uib-tooltip="{{ wbiTooltip }}" tooltip-placement="{{ wbiTooltipPlacement }}" tooltip-class="wb-input-state-tooltip" ng-if=" wbiTooltip "></span>' +
                        '<span class="input-state-spinner" ng-if="wbInputState === \'LOADING\'"><div spinner="" show="true"></div></span>' +
                        '<span class="input-state-icon" ng-if="wbInputState === \'SUCCESS\' || wbInputState === \'ERROR\'" ng-class="{ \'show-success-icon\': ( wbiShowSuccessIcon && wbInputState === \'SUCCESS\' && ngModel.length ), \'state-icon-success\': ( wbInputState === \'SUCCESS\' ), \'state-icon-error\': ( ! wbInputState || wbInputState === \'ERROR\' ) }"></span>' +
                    '</span>' );

                    if ( $scope.wbiTooltip ) {
                        $( $element ).addClass( 'padding-with-tooltip' );
                    }

                    $compile( $( $element ).parent( '.wb-input-state' ).find( '.status-container' ).contents() )( $scope );
                }



                function setupWatchModel () {
                    $scope.$watch( 'wbInputState', function () {
                        if ( $scope.wbInputState === 'ERROR' ) {
                            $( $element ).parent( '.wb-input-state' )
                                .addClass( 'wb-input-state-error' )
                                .removeClass( 'wb-input-state-success' );
                        } else {
                            $( $element ).parent( '.wb-input-state' )
                                .removeClass( 'wb-input-state-error' )
                                .addClass( 'wb-input-state-success' );
                        }
                    } );
                }



                setup();
            }
        };
    }



    angular.module( 'commons.directives' )
        .directive( 'wbInputState', wbInputState );
}());
