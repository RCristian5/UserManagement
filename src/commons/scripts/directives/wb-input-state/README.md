# wb-input-status

Directiva que ayuda en la tarea de mostrar u ocultar el estado de los campos de texto.

## atriutos

Para inicializar la directiva solo se debe usar el atributo wb-input-state

```html
<input type="text" wb-input-state="{{ status }}">
```

## atributos

`wb-input-state`: _default: `NONE`_\
es el indicador de la directiva a demás de que es el que
asignará el estado al input.

**estados**

>`SUCCESS`:\
>el estado success indica que el campo se ha completado correctamente.
>
>`ERROR`:\
>este estado indica que se tiene un error en el campo
>
>`LOADING`:\
>este estado indica que se está comprobando la información que el
>usuario ingreso al campo. Este estado es muy útil cuando se tienen que hacer
>peticiones al back para poder comprobar el dato del campo.\
>
>`NONE`:\
>este estado indica que no se tiene ninugun estatus definido

`wbi-show-success-icon`: _default: `false`_\
este atributo es solo la bandera que indica si se forzará a mostrarse
el ícono que indica success.

`wbi-tooltip`: _default: `undefined`_\
Texto que se pondrá en los mensajes de los tooltips

`wbi-tooltip-placement`: _default: `undefined`_\
Posición en la que se deberá colocar el tooltip y debido a que solo se tienen
dos lugares en específico en donde se pueden colocar los tooltips solo se
usan los siguientes dos:

>`top`: posiciona el tooltip en la parte superior del campo de texto\
>
> `right`: posiciona el tooltip en la parte derecha del campo de texto

## ejemplo de uso

```html
<form name="DataForm">
    <input type="text" name="titularName" ng-model="model.titular.nombre" wb-input-state="SUCCESS" wbi-show-success-icon="true" wbi-tooltip="welcome to hell you fucking droid !!! ..." wbi-tooltip-placement="right" />
</form>
```

```javascript

function MyController ( $scope ) {

    if ( $scope.DataForm.titularName.$valid ) {
        console.log( 'that\'s right !!! ...' );
    }
}

```
