(function(){
    'use strict';



    function radioButtons() {



        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'directives/checkbox/check-box.tpl.html',
            scope:{
                title: "@",
                description: "@",
                checked: "=checked",
                ngDisabled: "="
            },
            link:function ( $scope ){



                $scope.$watchGroup(['checked', '_master'], function(newValues, oldValues, scope) {
                    scope._master = scope.checked;
                });




            }



        };



    }



    angular.module( 'commons.directives' )
    .directive( 'checkBox', radioButtons );



}());
