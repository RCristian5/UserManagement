# Radio Buttons

### Para utilizar la directiva "check-box"
Insertar

```html
check-box( title="nissan march" description="1.6l active aa estandar hatchback 4 cil 5 p 5 ocup" on-click= "updateModelorigin" ng-disabled="false" checked="false" )
```

Donde

  - title ="" - El titulo o nombre del auto
  - description ="" - La descripción o atributos del vehiculos.
  - on-click ="" - Se pasa el nombre del la funcion definida en el scope del controlador y sera la que se ejecutara al darle click al radio-button
  - checked = "" - Se pasa el valor "true" para que el elemento aparezca seleccionado, o "false" en caso contrario, si se evita este atributo se tomara con un valor "false"
  - ng-disabled ="" - Se pasa el valor "true" para desabilitarlo o "false" para caso contrario, si se evita este atributo se tomara con un valor "false", si se evita este atributo se tomara con un valor "false"





