


(function () {
    'use strict';



    function AppConsSrv () {



        var api = {};



        api.status = {
            SUCCESS: 'SUCCESS',
            ERROR: 'ERROR',
            DONE: 'DONE',
            LOAD: "LOADING",
            INIT: "INITIAL",
            OK: 'OK'
        };



        return api;
    }



    angular.module( 'userManagement.services' )
        .constant( 'AppConsSrv', AppConsSrv );
}());
