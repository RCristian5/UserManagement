(function(){
    'use strict';



    function searchUsersCtrl ( $scope ){



        function setup (){
            setupVars();
        }



        $scope.goAddUser = function () {
            $scope.goView("add-user");
        };


        $scope.selectApp = function ( _selectApp ) {
            console.log(_selectApp);
            console.log($scope.optionSelectApp);
        };



        function setupVars () {
            $scope.optionSelectApp = {};
            $scope.apps = [
                { descripcionInterna: "Todas" },
                { descripcionInterna: "Gestion de usuarios" },
                { descripcionInterna: "Cupones" }
            ];
        }



        setup();
    }



    angular.module( "userManagement.controllers", [])
    .controller( "searchUsersCtrl", searchUsersCtrl );



})();
